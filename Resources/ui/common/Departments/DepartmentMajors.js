//Departments Window

// create var for the currentWindow  
var currentWin = Ti.UI.currentWindow;

// set the data from the database to the array  
function setData() {
	
	//Catch passed var
	var departmentMajors = Ti.UI.currentWindow.departmentMajors 
	
	//Database Connection
    var db = Ti.Database.install('../TAMUSA_APP.sqlite','MAJORS');  
    var rows = db.execute('SELECT DISTINCT MajorsID, MajorsName FROM MAJORS WHERE DepartmentsID="' + departmentMajors + '"'); 
	
	//Loop through the records and set the Table title using FieldByName
	var dataArray = [];  
	while (rows.isValidRow())  
	{  
    	dataArray.push({title:'' + rows.fieldByName('MajorsName') + '', hasChild:true, path:'MajorDetails.js', ID:'' + rows.fieldByName('MajorsID') + ''});  //Next File Path
    	rows.next();  
	};
	
	//Attach array to tableView
	tableview.setData(dataArray); 
};
 
// create table view  
var tableview = Ti.UI.createTableView({  
});

tableview.addEventListener('click', function(e)  
{ 
    if (e.rowData.path)  
    {
        //Create Dynamic window to contain the details
        var win = Ti.UI.createWindow({
        	backgroundColor:'#000',
            url:e.rowData.path,  
            title:'Concentrations'  
        });  
        
        //Pass values to next page
        var majorDetails = e.rowData.ID;  
		win.majorDetails = majorDetails; 
        
        //Open the Window  
        win.open();  
    }  
});

// add the tableView to the current window  
currentWin.add(tableview);

// call the setData function to attach the database results to the array  
setData();