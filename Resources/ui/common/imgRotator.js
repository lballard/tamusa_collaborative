var view = Titanium.UI.createImageView({
    height: 'auto',
    width: 320,
    zindex: 30
 
});
win.add(view);
 
var slides = [];
var slide1 = {img: 'images/10.jpg', displayTime: 3000};
var slide2 = {img: 'images/11.jpg', displayTime: 3000};
var slide3 = {img: 'images/12.jpg', displayTime: 3000};
var slide4 = {img: 'images/s2.png', displayTime: 3000}; // create in a Loop
slides.push(slide1);
slides.push(slide2);
slides.push(slide3);
slides.push(slide4);
 
var idx = 0;
var countSlides = slides.length;
var getNextSlide = function() {
idx = idx > countSlides ? 0 : idx;
var slide = slides[idx++];
return slide;
}
var showSlide = function(slide) {
view.backgroundImage = slide.img;
setTimeout(function() {
showSlide(getNextSlide());
}, slide.displayTime);
}
 
 
 
showSlide(getNextSlide());