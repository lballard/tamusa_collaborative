//FirstView Component Constructor
function FirstView() {
	
	//load component dependencies
	Ti.include('SocialMedia.js');
	
	//create object instance, a parasitic subclass of Observable
	var self = Ti.UI.createView();
	
	//Function to display Toasts when buttons are pressed
	function toastImages (messageText){
		var toast = Titanium.UI.createNotification({
    		duration: Ti.UI.NOTIFICATION_DURATION_SHORT,
    		message: messageText
  		});
  		toast.show();
	};
	
	//Function that displays a webview for each button
	function navWebview (link){
		var webview = Ti.UI.createWebView({url:link});
    	var window = Ti.UI.createWindow();
    	window.orientationModes = [Ti.UI.LANDSCAPE_RIGHT];
    	window.add(webview);
    	window.open({modal:true});
	};
	
	//Declare data variable
	var data =[];
	
	//Create table row 1, 2, and 3
	for(var i = 0; i < 1; i++) {
    	var row = Ti.UI.createTableViewRow();
        row.add(createButtonRow1());
        data.push(row);
    }
    for(var i = 0; i < 1; i++) {
    	var row = Ti.UI.createTableViewRow();
        row.add(createButtonRow2());
        data.push(row);
    }
    for(var i = 0; i < 1; i++) {
    	var row = Ti.UI.createTableViewRow();
        row.add(createButtonRow3());
        data.push(row);
    }
    
    //Create table view and append data
    var table = Ti.UI.createTableView({
    	data:data
    });
       
    //Function for first row
    function createButtonRow1(){
		//Create Button 1
		var image1 = Ti.UI.createButton({
			backgroundImage:"/images/Directory.png",
			width:160,
			height:180
		});
		
		//Add Event Listener to Button 1
		image1.addEventListener('click', function(e){
			toastImages('Directory');
			navWebview('http://www.tamusa.tamus.edu/PeopleSearch/PeopleSearch/default.aspx');
		});
        
        //Create Button 2
		var image2 = Ti.UI.createButton({
			backgroundImage:"/images/Library.png",
			width:160,
			height:180,
			left:20,
			right:20
		});
		
		//Add Event Listener to Button 2
		image2.addEventListener('click', function(e){
			toastImages('Library');
			navWebview('http://www.tamusa.tamus.edu/library/index.html');
		});
		
		//Create Button 3
		var image3 = Ti.UI.createButton({
			backgroundImage:"/images/Advisors.png",
			width:160,
			height:180
		});
		
		//Add Event Listener to Button 3
		image3.addEventListener('click', function(e){
			toastImages('Advisors');
			navWebview('http://www.tamusa.tamus.edu/advising/');
		});
		
		//Create View to contain buttons on Row 1
		var view = Ti.UI.createView({
			width:Ti.UI.SIZE,
			height:Ti.UI.SIZE,
        	layout: 'horizontal',
        	top:140
		});
        
        //Add views to first row
        view.add(image1);
        view.add(image2);
		view.add(image3);
		
		return view;
	}
	
	//Function for second row
	function createButtonRow2(){
		
		//Create Button 4
		var image4 = Ti.UI.createButton({
			backgroundImage:"/images/Courses.png",
			width:160,
			height:180
		});
		
		//Add Event Listener to Button 4
		image4.addEventListener('click', function(e){
			toastImages('Courses');
			navWebview('http://www.tamusa.tamus.edu/jaguarconnect/index.html');
		});
		
		//Create Button 5
		var image5 = Ti.UI.createButton({
			backgroundImage:"/images/News.png",
			width:160,
			height:180,
			left:20,
			right:20
		});
		
		//Add Event Listener to Button 5
		image5.addEventListener('click', function(e){
			toastImages('News');
			navWebview('http://www.tamusa.tamus.edu/universitycommunications/news/archives.html');
		});
		
		//Create Button 6
		var image6 = Ti.UI.createButton({
			backgroundImage:"/images/Maps.png",
			width:160,
			height:180
		});
		
		//Add Event Listener to Button 6
		image6.addEventListener('click', function(e){
			toastImages('Maps');
			navWebview('http://www.tamusa.tamus.edu/map.html');
		});
		
		var view = Ti.UI.createView({
			width:Ti.UI.SIZE,
			height:Ti.UI.SIZE,
        	layout: 'horizontal',
        	top:45,
        	marginBottom:140
		});
        
        //Add views to second row
		view.add(image4);
		view.add(image5);
        view.add(image6);
 
		return view;
	}
	
	//Function for third row
	function createButtonRow3(){
		
		//Create Button 7
		var image7 = Ti.UI.createButton({
			backgroundImage:"/images/GetHelp.png",
			width:160,
			height:180
		});
		
		//Add Event Listener to Button 7
		image7.addEventListener('click', function(e){
			toastImages('Get Help');
			navWebview('http://www.tamusa.tamus.edu/ITS/index.html');
		});
		
		//Create Button 8
		var image8 = Ti.UI.createButton({
			backgroundImage:"/images/Departments.png",
			width:160,
			height:180,
			left:20,
			right:20
		});
		
		//Add Event Listener to Button 8
		image8.addEventListener('click', function(e){
			toastImages('Departments');
			//navWebview('http://www.tamusa.tamus.edu/schools.html');
			//Create Dynamic window to contain the details
        	var win = Ti.UI.createWindow({
        		backgroundColor:'#000',
            	url:'Departments/Departments.js',  
            	title:'Departments'  
        	});  
        
        	//Pass values to next page
        	//var departments = e.rowData.ID;  
        	//win.departments = departments;
        
        	//Open the Window  
        	win.open();
		});
		
		//Create Button 9
		var image9 = Ti.UI.createButton({
			backgroundImage:"/images/SocialMedia.png",
			width:160,
			height:180
		});
		
		//Add Event Listener to Button 9
		image9.addEventListener('click', function(e){
			toastImages('Social Media');
			tableSocialMedia();  //Call the Social Media function
		});
		
		//Add views to third row
		var view = Ti.UI.createView({
			width:Ti.UI.SIZE,
			height:Ti.UI.SIZE,
			layout: 'horizontal',
			top:45
		});
		
		//Add views to third view
		view.add(image7);
		view.add(image8);
		view.add(image9);
		
		return view;
	}
    
	self.add(table);
	
	/*var menu = Titanium.UI.Android.createMenu();
	var item1 = Titanium.UI.Android.createMenuItem({
    	title : 'Item 1',
	});
	item1.addEventListener('click', function(){
    	Ti.UI.createAlertDialog({ title : 'You clicked Item 1'}).show();
	});
	menu.add(item1);
	Titanium.UI.Android.setMenu(menu);*/
	
	return self;
}

module.exports = FirstView;
