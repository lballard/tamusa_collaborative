function tableSocialMedia() {
	
	//Create New Tab Group
	var tabGroup = Ti.UI.createTabGroup();
	
	//Create new Social Media Windows
	var winYouTube = Ti.UI.createWindow({
		backgroundColor:'#3D3242',
		//_parent:Titanium.UI.currentWindow,
		modal:true,
		exitOnClose:false,
		orientationModes: [Ti.UI.PORTRAIT]
		
	});
	
	var winFacebook = Ti.UI.createWindow({
		backgroundColor:'#3D3242',
		//_parent:Titanium.UI.currentWindow,
		modal:true,
		exitOnClose:false,
		orientationModes: [Ti.UI.PORTRAIT]
		
	});
	
	var winTwitter = Ti.UI.createWindow({
		backgroundColor:'#3D3242',
		//_parent:Titanium.UI.currentWindow,
		modal:true,
		exitOnClose:false,
		orientationModes: [Ti.UI.PORTRAIT]
		
	});
	
	//Window Landscape Mode
	winYouTube.orientationModes = [Ti.UI.PORTRAIT];
	winFacebook.orientationModes = [Ti.UI.PORTRAIT];
	winTwitter.orientationModes = [Ti.UI.PORTRAIT];
	
	//Create Social Media Tabs
	var tabYouTube = Ti.UI.createTab({
    	window:winYouTube,
    	title:'YouTube'
	});
	
	var tabFacebook = Ti.UI.createTab({
    	window:winFacebook,
    	title:'Facebook'
	});
	
	var tabTwitter = Ti.UI.createTab({
    	window:winTwitter,
    	title:'Twitter'
	});
	
	
	//Create Web Views
	var youTube = Ti.UI.createWebView({
		url:'http://www.youtube.com/user/tamusavids',
		backgroundColor:'#3D3242'
	});
	
	var facebook = Ti.UI.createWebView({
		url:'https://www.facebook.com/TAMUSanAntonio',
		backgroundColor:'#3D3242'
	});
	
	var twitter = Ti.UI.createWebView({
		url:'https://twitter.com/TAMUSanAntonio',
		backgroundColor:'#3D3242'
	});
	
	//Add the Web Views to the Windows
	winYouTube.add(youTube);
	winFacebook.add(facebook);
	winTwitter.add(twitter);
		
	/*// create table view data object
	var data = [
		{title:'YouTube Channel', hasChild:true, url:'http://www.youtube.com/user/tamusavids'},
		{title:'Facebook', hasChild:true, url:'https://www.facebook.com/TAMUSanAntonio'},
		{title:'Twitter', hasChild:true, url:'https://twitter.com/TAMUSanAntonio'}
	];
		
	// create table view
	for (var i = 0; i < data.length; i++ ) { 
		data[i].color = '##eecd86';
		data[i].font = {fontWeight:'bold', fontSize:'40px'} 
	};
	
	//Add data to table view	
	var socialTableView = Ti.UI.createTableView({
		data:data
	});
		
	// create table view event listener
	socialTableView.addEventListener('click', function(e) {
    	if (e.rowData) {
    		
    		//create web view
 			var newView = Ti.UI.createWebView({
            	url:e.rowData.url,
            	title:e.rowData.title
        	});
        }
    	
    	//Add web view to the social table view
    	socialTableView.add(newView, {modal:true});
	});
	
	//Create the close button
	var close = Titanium.UI.createButton({
        title:'Close'
        //style:Titanium.UI.iPhone.SystemButtonStyle.PLAIN
    });
    
    //Add the close button to the window
    window.add(close);
    
    //Add a click event listener
    close.addEventListener('click',function(){
        window.close();
    });*/
    
    //Add the tabs to the Tab Group
	tabGroup.addTab(tabYouTube);
	tabGroup.addTab(tabFacebook);
	tabGroup.addTab(tabTwitter);
    
    //Open the Tab Group
	tabGroup.open();
        
	//return window;
	//return tabGroup;
	
};